    <!--[if !IEMobile]><!-->
        <link href="styles/style.css" rel="stylesheet" type="text/css" />
        <script src="scripts/jquery.lwtCountdown-1.0.js" type="text/javascript"></script>
        <script src="scripts/bind.js" type="text/javascript"></script>
    
        <script type="text/javascript">
            $(window).ready(function () {
    
                loadContent("subscribe");
                
                var dt = getCompanyDeadline();
                
                $("#countdown_dashboard").countDown({
                    targetDate: {
                        'day': dt.getDate(),
                        'month': dt.getMonth() +1,
                        'year': dt.getFullYear(),
                        'hour': dt.getHours(),
                        'min': dt.getMinutes(),
                        'sec': 0
                    }
                });
                
                var companyName = getCompanyName();
                document.title = "Em Construção - " + companyName;
                $("#wrap > h1").text(companyName);
    
            });
        
        </script>
    
    <!--<![endif]-->
    
    <!--[if IEMobile]>
        <script src="scripts/mobile.js" type="text/javascript"></script>
        <link href="styles/mobile.css" rel="stylesheet" type="text/css" />
    
        <script type="text/javascript">
            $(window).ready(function () {
    
                loadContent("subscribe");
                
                var dt = getCompanyDeadline();
                
                var companyName = getCompanyName();
                document.title = "Em Construção - " + companyName;
                $("#wrap > h1").text(companyName);
    
            });
        
        </script>    
    
    <![endif]-->
    
    