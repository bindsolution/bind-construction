<?php

// getting variables from form
$email = trim($_REQUEST['email']);
$filename = $_SERVER['DOCUMENT_ROOT'] . "/data/subscribers.xml";

$doc = new DOMDocument(); 
$doc->formatOutput = true;

if (file_exists($filename)) {
    $doc->load($filename); 
    $root = $doc->documentElement;
}
else {
    $root = $doc->createElement("subscribers");
    $doc->appendChild($root);
}

$sub = $doc->createElement("subscriber");
$root->appendChild($sub);

$emailNode = $doc->createTextNode($email);
$sub->appendChild($emailNode);

$doc->save($filename);

echo '<p>Obrigado por se inscrever!</p>';
echo '<p>Enviaremos um email assim que o site estiver finalizado.</p>';

die();
?>​