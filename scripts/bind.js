$(window).ready(function () {

    $("a#back, #start-page").click(function(){
        loadContent("subscribe");
    }); 
    
});

function loadFacebook()
{
    var url = "https://www.facebook.com/pages/Bind-Solution/159715344077728";
    $('#facebookHolder').append('<div id="fb-root"></div>'); 
    $('#facebookHolder').append('<fb:like-box href="' + url + '" width="350" show_faces="true" stream="false" header="false"></fb:like-box>'); 

    jQuery.getScript('http://connect.facebook.net/en_US/all.js#xfbml=1', function() { 
        FB.init({status: true, cookie: true, xfbml: true}); 
        sizeWindows();
    }); 
}
function isValidEmailAddress(emailAddress) 
{
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}


function sizeWindows(){
    //Size Windows
    var h = $("div#wrap").height();
    h = ((h / 2) * -1) - 25;
    $("div#floater").css("margin-bottom", h + "px");
}

function loadContent(pageName)
{
    $.ajax({
        type: "GET",
        url: pageName + ".htm",
        dataType: "html",
        success: function (data) {
            $("#content").hide();
            $("#content").empty();
            $("#content").html(data).fadeIn();
            
            if (pageName == 'social')
            {
                loadFacebook();
            }
            else
            {
                sizeWindows();
            }
            
        }
    });
}